
Requires MongoDB to be installed on local machine (https://docs.mongodb.org/manual/installation/)

To get up and running locally:
```
git clone git@github.com:jeffjones89/photofinder.git
```
```
npm install
```
Once dependencies are installed, ensure that an instance of Mongod is running in the background. Then from the root of the project seed the database using:
```
seed
```

If an error is thrown, globally install the node-mongo-seeds package using:

```
npm install -g node-mongo-seeds

IMPORTANT: Ensure that an instance of mongod is running in the background in order to communicate with your local MongoDB by running
```
mongod
```
or by opening the mongod.exe program if the mongo bin is not in your default path
```
start the server with node or using nodemon
```
nodemon
```
The server is listening on port 3000 (http://localhost:3000)

 $(document).ready(function() {
  //submit event firing to server
  $('#emailForm').submit(function(event) {
    event.preventDefault()
    var emailInput = $('[name = email]');
    $('.x').hide();

    $('.photoContainer').empty();
    var emailAddress = emailInput.val()
    if(emailAddress!=""){
      $('#search').on('click', function(){
        $('.x').hide(); //x: class of things to hide when you push search
     });
    }
    // clear input field
    emailInput.val('');
      //ajax call to return Full Contact API data
    $.ajax({
      method: "POST",
      url: "/api/fullcontact/visitors",
      data: {
        email: emailAddress
      }
    }).done(function(response) {
      console.log(response);
      var photos= response.photos;

      //change header
      $('h1').text("Search Results");

      $('.emailAddress').text(emailAddress);

      console.log(photos)
      if(!photos){
        console.log("no photo found")
        //add class fail

        $('.flashMessage').addClass("fail");
        $('.flashMessage').html("Sorry. No photos found for that email.")
      }

      $.each(photos, function(index, photo){
        $('.photoContainer').append('<img src="' + photo.url + '"' +'class = "photo"' + '>')
      })
      $('#searchAgain').toggle({
        duration: 500
      });
    });
  });

  //display new employee form
  $('[name = addEmployee]').on('click', function() {
    $('.newEmployee').toggle({
      duration: 500
    });
  });

  $('#newBCE').submit(function(event) {
    //capture user inputs for req.body
    event.preventDefault();
    var firstName = $('#firstName').val();
    var lastName = $('#lastName').val();
    var email = $('#email').val()
    var photoUrl = $('#photos').val()
      //ajax call to employee server
    $.ajax({
      method: "POST",
      url: "/api/employees",
      data: {
        email: email,
        firstName: firstName,
        lastName: lastName,
        photos: [{
          url: photoUrl
        }]
      }
    }).done(function(response) {
      console.log(response);
      var message="&#10003; Thank you! " + firstName + " has been added.";
      $('.flashMessage').addClass("success");
      //add check mark
      $('.flashMessage').html(message);
      $("#newBCE")[0].reset();
      // $('.flashMessage').toggle({
      //   duration: 500
      // });
      //clear form


    })


  })

});

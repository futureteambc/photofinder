//query full contact api and check for existing BC employee
var config = require('../config.js');
var request = require('request');
var fcKey = config.fullContact.key;
var Employee = require('../models/employee');
var knownVisitor = require('../models/knownVisitor');
var notification = require('./notificationController');
exports.getVisitorInfo = function(req, res, next) {
  var index = req.body.email.indexOf('@bytecubed.com')
    //determine if BC employee, then return object from mongodb;
  if (index > -1) {
    Employee.findOne({
      'email': req.body.email
    }, function(err, employee) {
      res.json(employee || {flash: 'This employee has not been added to the database. Please add them from the home page.'});
    });
  } else {
    knownVisitor.findOne({
      'email': req.body.email
    }, function(err, knownVisitor) {
      //query known visitor DB and return object if exists
      if (knownVisitor) {
        res.json(knownVisitor);
        notification.sendEmail(knownVisitor);
        notification.sendText(knownVisitor);
      } else {
        //if not employee or known visitor, query Full Contact API;
        request('https://api.fullcontact.com/v2/person.json?email=' + req.body.email + '&apiKey=' + fcKey, function(err, response, body) {
          if (!err && res.statusCode == 200) {
            var unknownVisitor = JSON.parse(body);
            console.log(unknownVisitor);
            res.json(unknownVisitor);
            //check to see if photos array exists, if so send notification -- probably not real world applicable
            if(unknownVisitor.photos){
              notification.sendEmail(unknownVisitor);
              notification.sendText(unknownVisitor);
            }
          } else {
            res.send(404);
          }
        });
      }
    });

  }
}

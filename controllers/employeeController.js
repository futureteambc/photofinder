//using mongoose to create new employee object
var Employee = require('../models/employee');
exports.postVisitors = function(req, res) {
  var index = req.body.email.indexOf('@bytecubed.com')
  if(index > -1 ){
    var employee = new Employee();
    employee.firstName = req.body.firstName;
    employee.lastName = req.body.lastName;
    employee.email = req.body.email;
    employee.photos = req.body.photos
    employee.save(function(err) {
      if (err)
        res.send(err)
      res.json({
        message: 'Employee added!',
        data: employee
      });
    });
  } else {
    res.json({message: 'Sorry, this is does not match the ByteCubed email domain'});
  }

}

var config = require('../config.js');
var twilio = require('twilio')(config.twilio.sid, config.twilio.token);
var sendgrid = require('sendgrid')(config.sendgrid.key);
var notification = {};
notification.sendEmail = function(data) {
  //contactInfo is a property of the object that the Full Contact API returns. If coming from API, use best guessed full name, if not use data from employee/known visitor list
  if (data.contactInfo) {
    var name = data.contactInfo.fullName;
  } else {
    var name = data.firstName + ' ' + data.lastName;
  }
  console.log(name);
  var message = {
    to: 'bcfutureteam@gmail.com', //replace with Outlook currentuser.email
    from: 'charles.jones@bytecubed.com', //replace with virtualassistant@bytecubed.com
    subject: name + ' has arrived!',
    text: name + ' is in the lobby for your 10:30am meeting.'
  }
  sendgrid.send(message, function(err, json) {
    if (err) {
      console.error(err);
    }
    console.log(json);
  });
}

notification.sendText = function(data) {
  if (data.contactInfo) {
    var name = data.contactInfo.fullName
  } else {
    var name = data.firstName
  }
  //instantiate twilio message
  twilio.messages.create({
    to: "+12026309367", //replace with Outlook user data?
    from: config.twilio.number,
    body: name + ' is here for your 4:00pm meeting!'
  }, function(err, message) {
    if (err) {
      console.error(err);
    }
    console.log({
      message: 'success!'
    });
  });
}

module.exports = notification;

var mongoose = require('mongoose');
var employeeSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
    unique: true
  },
  firstName: {
    type: String,
    required: true
  },
  lastName: {
    type: String,
    required: true
  },
  photos: [{
    url: String,
    _id: false
  }]
});

var employeeModel = mongoose.model("Employee", employeeSchema);
module.exports = employeeModel

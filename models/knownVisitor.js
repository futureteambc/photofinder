var mongoose = require('mongoose');
var knownVisitorSchema = new mongoose.Schema({
  firstName: {
    type: String
  },
  lastName: {
    type: String
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  photos: [{
    url: String,
    _id: false
  }],
  meetings: [{
    date: Date,
    content: String,
    _id: false
  }]
});

var knownVisitorModel = mongoose.model("KnownVisitor", knownVisitorSchema);
module.exports = knownVisitorModel;
